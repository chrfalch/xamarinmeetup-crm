﻿using System;
using Microsoft.WindowsAzure.MobileServices;
using Microsoft.WindowsAzure.MobileServices.Sync;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace com.xamarinmeetup.crm.data
{
	/// <summary>
	/// Implements a generic repository acting on a generic model. The repository
	/// reads and writes to a Mobile Service (Azure) and uses a local storage for
	/// offline support.
	/// </summary>
	public class Repository<TModel>
	{
		#region Private Members

		/// <summary>
		/// The mobile services client
		/// </summary>
		private readonly IMobileServiceClient _client;

		/// <summary>
		/// The Mobile Services Sync table
		/// </summary>
		private IMobileServiceSyncTable<TModel> _table;

		/// <summary>
		/// Local store for offline support
		/// </summary>
		private IMobileServiceLocalStore _localStore;

		/// <summary>
		/// Initialized flag for lazy initialization
		/// </summary>
		private bool _initialized = false;

		/// <summary>
		/// Singleton instance
		/// </summary>
		private static Repository<TModel> _repository;

		#endregion

		#region Static Members

		/// <summary>
		/// Initializes the repository singleton with a local store that must be implemented
		/// and instantiated by a platform specific instance.
		/// </summary>
		public static void Initialize(IMobileServiceClient client, IMobileServiceLocalStore store)
		{
			if (_repository != null)
				return;
				
			_repository = new Repository<TModel> (client, store);
		}

		/// <summary>
		/// Singleton accessor property
		/// </summary>
		/// <value>The current.</value>
		public static Repository<TModel> Current
		{
			get
			{
				if (_repository == null)
					throw new InvalidOperationException ("Call initialize before retrieving the repo");

				return _repository;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="com.xamarinmeetup.crm.data.Repository`1"/> class.
		/// </summary>
		private Repository (IMobileServiceClient client, IMobileServiceLocalStore localStore)
		{
			_client = client;
			_localStore = localStore;
			_initialized = false;
		}

		#endregion

		#region Private Members

		/// <summary>
		/// Lazy initialization method. Called internally
		/// </summary>
		/// <returns>The local store.</returns>
		private async Task InitLocalStore()
		{
			if (_initialized)
				return;

			await _client.SyncContext.InitializeAsync (_localStore);
			await _client.SyncContext.PushAsync ();
			_table = _client.GetSyncTable<TModel> ();

			_initialized = true;
		}

		#endregion

		/// <summary>
		/// Synchronizes online/offline
		/// </summary>
		/// <returns>The data async.</returns>
		public async Task RefreshDataAsync()
		{
			await InitLocalStore ();

			await _client.SyncContext.PushAsync ();
			await _table.PullAsync ();
		}

		/// <summary>
		/// Returns an IEnumerable of the model asynch from the offline or online store
		/// </summary>
		public async Task<IEnumerable<TModel>> GetItemsAsync()
		{
			await InitLocalStore ();

			await _table.PullAsync ();
			return await _table.ToEnumerableAsync ();
		}

		/// <summary>
		/// Adds a new instance of a model
		/// </summary>
		/// <returns>The async.</returns>
		/// <param name="model">Model.</param>
		public async Task AddAsync (TModel model)
		{
			await InitLocalStore ();

			await _table.InsertAsync(model);
		}			
	}
}
	