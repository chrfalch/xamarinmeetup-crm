﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using Microsoft.WindowsAzure.MobileServices.SQLiteStore;
using System.IO;

namespace com.xamarinmeetup.crm.app.android
{
	[Activity (Label = "CRM", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : AndroidActivity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			Forms.Init (this, bundle);

			App.Startup (CreateLocalStorage ());

			// Starts with the main Xamarin.Forms screen
			SetPage (App.GetMainPage());
		}

		/// <summary>
		/// Creates and sets up the local store for the mobile service offline support
		/// </summary>
		/// <returns>The local storage.</returns>
		private MobileServiceSQLiteStore CreateLocalStorage ()
		{
			// new code to initialize the SQLite store
			var path = Path.Combine(System.Environment.GetFolderPath(
				System.Environment.SpecialFolder.Personal), "test1.db");

			if (!File.Exists(path))			
				File.Create(path).Dispose();

			// These two methods needs to be called on the platform side of the app
			Microsoft.WindowsAzure.MobileServices.CurrentPlatform.Init ();
			// SQLitePCL.CurrentPlatform.Init ();

			return new MobileServiceSQLiteStore(path);
		}
	}
}


