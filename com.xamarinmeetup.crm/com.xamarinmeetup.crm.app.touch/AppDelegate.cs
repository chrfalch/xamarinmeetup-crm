﻿using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

using Xamarin.Forms;
using System.IO;
using Microsoft.WindowsAzure.MobileServices.SQLiteStore;
using Microsoft.WindowsAzure.MobileServices;
using com.xamarinmeetup.crm.data;

namespace com.xamarinmeetup.crm.app.touch
{
	// The UIApplicationDelegate for the application. This class is responsible for launching the
	// User Interface of the application, as well as listening (and optionally responding) to
	// application events from iOS.
	[Register ("AppDelegate")]
	public partial class AppDelegate : UIApplicationDelegate
	{
		// class-level declarations
		UIWindow window;

		//
		// This method is invoked when the application has loaded and is ready to run. In this
		// method you should instantiate the window, load the UI into it and then make the window
		// visible.
		//
		// You have 17 seconds to return from this method, or iOS will terminate your application.
		//
		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
			Forms.Init ();

			// create a new window instance based on the screen size
			window = new UIWindow (UIScreen.MainScreen.Bounds);

			// Initialize the application with a local SQLite storage for offline support
			App.Startup (CreateLocalStorage ());

			// If you have defined a root view controller, set it here:
			window.RootViewController = App.GetMainPage().CreateViewController();
			
			// make the window visible
			window.MakeKeyAndVisible ();
			
			return true;
		}

		/// <summary>
		/// Creates and sets up the local store for the mobile service offline support
		/// </summary>
		/// <returns>The local storage.</returns>
		private MobileServiceSQLiteStore CreateLocalStorage ()
		{
			// new code to initialize the SQLite store
			var path = Path.Combine(System.Environment.GetFolderPath(
				System.Environment.SpecialFolder.Personal), "test1.db");

			if (!File.Exists(path))			
				File.Create(path).Dispose();

			// These two methods needs to be called on the platform side of the app
			Microsoft.WindowsAzure.MobileServices.CurrentPlatform.Init ();
			SQLitePCL.CurrentPlatform.Init ();

			return new MobileServiceSQLiteStore(path);
		}
	}
}

