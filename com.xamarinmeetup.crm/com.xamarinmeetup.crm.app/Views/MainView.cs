﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;
using com.xamarinmeetup.crm.data;
using System.Threading.Tasks;
using com.xamarinmeetup.crm.localization;

namespace com.xamarinmeetup.crm.app
{
	public class MainView: BaseView<MainViewModel>
	{
		public MainView ()
		{
			// Set up header with 'add item' ui
			var textField = new Entry{ Placeholder = Strings.TextFieldNewContactPlaceholder };
			textField.SetBinding (Entry.TextProperty, new Binding ("NewContactName"));

			var addButton = new Button{ Text = "+" };
			addButton.Command = ViewModel.AddContactCommand;

			var header = new Grid { Padding = new Thickness (4) };
			header.RowDefinitions = new RowDefinitionCollection{ 
				new RowDefinition{ Height = GridLength.Auto }
			};

			header.ColumnDefinitions = new ColumnDefinitionCollection{ 
				new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star) },
				new ColumnDefinition{ Width = GridLength.Auto }
			};

			header.Children.Add (textField, 0, 0);
			header.Children.Add (addButton, 1, 0);

			// Create list view
			var listView = new ListView { RowHeight = 50 };
			listView.ItemsSource = ViewModel.Contacts;

			listView.ItemTemplate = new DataTemplate(typeof(TextCell));
			listView.ItemTemplate.SetBinding(TextCell.TextProperty, "FirstName");

			// Fill content
			Content = new StackLayout{ 

				Padding = new Thickness(0),
				Children = {header, listView}
			};					
		}
	}
}

