﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using com.xamarinmeetup.crm.data;
using System.Collections.Generic;
using System.Windows.Input;
using Xamarin.Forms;
using com.xamarinmeetup.crm.localization;

namespace com.xamarinmeetup.crm.app
{
	/// <summary>
	/// Implements the main viewmodel for the app
	/// </summary>
	public class MainViewModel: BaseViewModel
	{
		#region Private Members

		/// <summary>
		/// Observable collection of contacts. Used for binding.
		/// </summary>
		private readonly ObservableCollection<Contact> _contacts = 
			new ObservableCollection<Contact> ();

		/// <summary>
		/// Backing field for the NewContactName property 
		/// </summary>
		private string _newContactName;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="com.xamarinmeetup.crm.app.MainViewModel"/> class.
		/// </summary>
		public MainViewModel ():base()
		{

		}

		#endregion

		#region View Lifecycle

		/// <summary>
		/// Overridden OnAppearing to start loading data when the GUI is ready.
		/// </summary>
		public override void OnAppearing ()
		{
			base.OnAppearing ();

			ReloadData ();
		}

		#endregion

		#region Private Members

		/// <summary>
		/// Method for reloading the data in the background and for updating the UI in 
		/// the main ui thread.
		/// </summary>
		/// <returns>The data.</returns>
		private Task ReloadData()
		{
			// Get the main uithread context
			var context = TaskScheduler.FromCurrentSynchronizationContext();

			// Run task that loads data in the background
			return Task.Run<IEnumerable<Contact>> (async () => {

				var contactList = await Repository<Contact>.Current.GetItemsAsync();
				return contactList;

			}).ContinueWith((t) =>{

				_contacts.Clear ();

				// UI-thread
				foreach(var contact in t.Result)
					_contacts.Add(contact);

			}, context);
		}

		#endregion

		#region ViewModel Properties

		/// <summary>
		/// Observable collection of the contact list
		/// </summary>
		/// <value>The contacts.</value>
		public ObservableCollection<Contact> Contacts { get { return _contacts; } }

		/// <summary>
		/// Returns the view title
		/// </summary>
		/// <value>The view title.</value>
		public override string ViewTitle {
			get {
				return Strings.AppTitle;
			}
		}

		/// <summary>
		/// Gets or sets the name of a new contact. Used when we execute the AddNewContact command
		/// </summary>
		/// <value>The new name of the contact.</value>
		public string NewContactName 
		{
			get
			{
				return _newContactName ?? string.Empty;
			}
			set
			{ 
				if (value == _newContactName)
					return;

				_newContactName = value;

				NotifyPropertyChanged(() => NewContactName);
			}
		}

		#endregion

		#region ViewModel Commands

		/// <summary>
		/// Returns a command that will add a new contact using the name in the 
		/// NewContactName property
		/// </summary>
		/// <value>The add contact command.</value>
		public ICommand AddContactCommand {
			get{
				return new Command (async () => {

					var names = NewContactName.Split (' ');
					var firstName = names.Length > 0 ? names [0] : string.Empty;
					var lastName = names.Length > 1 ? names[names.Length-1] : string.Empty;

					var newContact = new Contact{
						FirstName = firstName,
						LastName = lastName
					};

					await Repository<Contact>.Current.AddAsync(newContact);
					NewContactName = string.Empty;

					await Repository<Contact>.Current.RefreshDataAsync();

					await ReloadData();

				});
			}
		}

		#endregion
	}
}

