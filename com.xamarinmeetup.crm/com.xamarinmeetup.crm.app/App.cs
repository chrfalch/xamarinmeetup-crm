﻿using System;
using Xamarin.Forms;
using com.xamarinmeetup.crm.data;
using System.IO;
using Microsoft.WindowsAzure.MobileServices;
using Microsoft.WindowsAzure.MobileServices.Sync;
using Microsoft.WindowsAzure.MobileServices.SQLiteStore;
using com.xamarinmeetup.crm.localization;

namespace com.xamarinmeetup.crm.app
{
	public static class App
	{
		/// <summary>
		/// Returns the main page for the app
		/// </summary>
		/// <returns>The main page.</returns>
		public static Page GetMainPage(){

			return new NavigationPage (new MainView ());
		}

		/// <summary>
		/// Startup method that should be called before the GetMainPage method is called.
		/// Initializes and sets up the storage and services in the app.
		/// </summary>
		/// <param name="localStore">Local store.</param>
		public static void Startup(MobileServiceSQLiteStore localStore)
		{
            var client = new MobileServiceClient ("https://xamarinsep.azure-mobile.net/", 
				"gbQeumyvgUiwNmPEdbLzvWFOQRQECe67");
            var c = System.Globalization.CultureInfo.CurrentCulture;
            var cui = System.Globalization.CultureInfo.CurrentUICulture;

			Repository<Contact>.Initialize (client, localStore);
			localStore.DefineTable<Contact> ();
		}
	}
}

