﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Xamarin.Forms;
using Microsoft.WindowsAzure.MobileServices.SQLiteStore;
using System.IO;

namespace com.xamarinmeetup.crm.app.wp
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();

            // Sample code to localize the ApplicationBar
            Forms.Init();

            com.xamarinmeetup.crm.app.App.Startup(CreateLocalStorage());

            Content = com.xamarinmeetup.crm.app.App.GetMainPage().ConvertPageToUIElement(this);
        }

        /// <summary>
        /// Creates and sets up the local store for the mobile service offline support
        /// </summary>
        /// <returns>The local storage.</returns>
        private MobileServiceSQLiteStore CreateLocalStorage()
        {
            // new code to initialize the SQLite store
            var path = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, "test1.db");

            if (!File.Exists(path))
                File.Create(path).Dispose();

            // These two methods needs to be called on the platform side of the app
            // Microsoft.WindowsAzure.MobileServices.CurrentPlatform.Init();
            // SQLitePCL.CurrentPlatform.Init();

            return new MobileServiceSQLiteStore(path);
        }
    }
}